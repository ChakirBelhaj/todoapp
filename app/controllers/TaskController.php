<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/23/2017
 * Time: 10:49 PM
 */

namespace Todo\Controllers;

use Todo\Core\App;

class TaskController
{
    //shows error
    public function showErrorAddImage($array){
        return view('addtask', [
            'error' => $array
        ]);
    }

    //security checks if task is assigned to logged in user
    public function taskSecurity(){
        // there is no idtask redirect to /mytasks
        if(!$_GET['idtask']){
            header('Location: /mytasks');
            exit();
        }
        //gets task
        $objectTasks = App::get('database')->selectTaskOnId('tasks',$_GET['idtask']);
        //encodes and decodes to change an std object in an array, this to make looping possible
        $arrayTasks = json_decode(json_encode($objectTasks), True);
        //loops through arraytasks
        foreach($arrayTasks as $tasks){
            //if task is not assigned to session user, redirect.
            if($tasks['assigned_to'] != $_SESSION['iduser']){
                header('Location: /mytasks');
                exit();
            }
        }
    }

    //gets all task for logged in user
    public function getMyTasks(){
        if(!$_SESSION['loggedin']){
            header('Location: /login');
            exit();
        }

        $mytasks = App::get('database')->selectMyTasks('tasks',$_SESSION['iduser']);

        return view('mytasks', [
            'mytasks' => $mytasks
        ]);
    }

    //add tasks
    public function addTaskIndex(){
        return view('addtask');
    }

    // handles uploaded images and returns base64_encoded image
    private function handleProductImageUpload() {
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $file_type = $_FILES['image']['type'];
        $file_error = $_FILES['image']['error'];
        $expensions = array("image/jpeg", "image/jpg", "image/png", "image/gif", "image/bmp");

        if ($file_error !== UPLOAD_ERR_OK) {

            $error = 'There was an error uploading your file: ' . $file_error;
            return $this->showErrorAddImage($error);
        }

        if (in_array($file_type, $expensions) === false) {
            $error = "extension not allowed, please choose a JPEG or PNG file.";
            return $this->showErrorAddImage($error);
        }

        if($file_size > 2097152) {
            $error = 'File size must be smaller then 2 MB';
            return $this->showErrorAddImage($error);
        }
        // Get the image data
        return base64_encode(file_get_contents($_FILES['image']['tmp_name']));
    }

    //add task
    public function addTaskPost(){
        // The user uploaded an image
        if (isset($_FILES['image']) && !empty($_FILES['image']['tmp_name'])){
            // Returns a base64 encoded image, or a redirect response, so we check
            $image = $this->handleProductImageUpload();
            if (!is_string($image)) {
                return $image;
            }
        }

        // Only add image if it has been changed
        if(!empty($image)) {
            $product_data['image'] = $image;
        }else{
            $image = NULL;
        }

        App::get('database')->insert('tasks',[
            'title'         => $_POST['title'],
            'description'   => $_POST['description'],
            'assigned_to'   => $_SESSION['iduser'],
            'completed'     => '0',
            'created_at'    => date("Y/m/d"),
            'updated_at'    => date("Y/m/d"),
            'image'         => $image
        ]);
        $success[] = "A new task has been added";

        return view('addtask',[
            'success' => $success
         ]);
    }

    //removes task
    public function removeTask(){
        $this->taskSecurity();

        App::get('database')->deleteFromTask('tasks',$_GET['idtask']);
        $mytasks = App::get('database')->selectMyTasks('tasks',$_SESSION['iduser']);
        $success[]="Task has successfully been removed";
        return view('mytasks', [
            'mytasks'  => $mytasks,
            'success'  => $success
        ]);
    }

    //sets selected task to complete
    public function completeTask(){
        $this->taskSecurity();

        App::get('database')->completeTask(
            'tasks',
            $_GET['idtask'],
            date("Y/m/d")
        );
        $mytasks = App::get('database')->selectMyTasks('tasks',$_SESSION['iduser']);
        $success[] = "Task with id of " . $_GET['idtask'] . " has been completed";
        return view('mytasks', [
            'mytasks'  => $mytasks,
            'success'  => $success
        ]);
    }

    //sets task to incomplete
    public function undoCompleteTask(){
        $this->taskSecurity();

        App::get('database')->undoCompleteTask(
            'tasks',
            $_GET['idtask'],
            date("Y/m/d")
        );
        $mytasks = App::get('database')->selectMyTasks('tasks',$_SESSION['iduser']);
        $success[] = "Completed task with id of " . $_GET['idtask'] . " has been set to incomplete";
        return view('mytasks', [
            'mytasks'  => $mytasks,
            'success'  => $success
        ]);
    }

    //return page with task information
    public function checkTask(){
        $this->taskSecurity();

        $mytask = App::get('database')->selectTaskOnId('tasks',$_GET['idtask']);

        return view('checktask', [
            'mytask' => $mytask[0]
        ]);
    }

//    shows update task panel
    public function updateTask(){
        $this->taskSecurity();

        $mytask = App::get('database')->selectTaskOnId('tasks',$_GET['idtask']);

        return view('updatetask', [
            'mytask' => $mytask[0]
        ]);
    }

    //updates task
    public function postUpdateTask(){
        $this->taskSecurity();

        // The user uploaded an image
        if (isset($_FILES['image']) && !empty($_FILES['image']['tmp_name'])){
            // Returns a base64 encoded image, or a redirect response, so we check
            $image = $this->handleProductImageUpload();
            if (!is_string($image)) {
                return $image;
            }
        }

        // Only add image if it has been changed
        if(!empty($image)) {
            $product_data['image'] = $image;
        }else{
            $image = NULL;
        }

        App::get('database')->updateTask(
            'tasks',
            $_POST['idtask'],
            $_POST['title'],
            $_POST['description'],
            date("Y/m/d"),
            $image
        );

        $mytask = App::get('database')->selectTaskOnId('tasks',$_GET['idtask']);
        $success[] = 'Your task has been updated';
        return view('updatetask', [
            'mytask' => $mytask[0],
            'success' => $success
        ]);
    }

    //show reassign page
    public function reassignIndex(){
        $this->taskSecurity();

        $users = App::get('database')->selectAllExceptFromLoggedinUser('users',$_SESSION['iduser']);

        return view('reassign', [
            'users' => $users
        ]);
    }

    //reassigns task
    public function reassignTask(){
        $this->taskSecurity();

        App::get('database')->reassigntask(
            'tasks',
            $_GET['idtask'],
            $_GET['iduser'],
            date("Y/m/d")
        );

        $mytasks = App::get('database')->selectMyTasks('tasks',$_SESSION['iduser']);
        $success[] = "Task with task id " . $_GET['idtask'] . " has been reassigned to user with user id  " . $_GET['iduser'];
        return view('mytasks', [
            'mytasks' => $mytasks,
            'success' => $success
        ]);
    }


}