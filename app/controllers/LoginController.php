<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/23/2017
 * Time: 3:37 PM
 */

namespace Todo\Controllers;

use Todo\Core\App;

class LoginController
{
    //show error
    public function showError($array){
        return view('login', [
            'errors' => $array
        ]);
    }
    //show success
    public function showSuccess($array){
        return view('loggedin', [
            'success' => $array
        ]);
    }
    //returns login page
    public function getLogin(){
        return view('login');
    }

    //posts login information
    public function postLogin(){
        $user = App::get('database')->selectUser($_POST['name']);
        if(! password_verify($_POST['password'], $user[0]->password)){
            $errors[] = "Username or password is invalid";
            return $this->showError($errors);
        }else{
            //change stdobject to array
            $array = json_decode(json_encode($user[0]), True);
            $_SESSION = $array;
            $_SESSION['loggedin'] = 1;
            $success[] = "You have been successfully logged in. <br> You will be redirected...";
            return $this->showSuccess($success);
        }
    }
    //destroys session and logs the user out
    public function logout(){
        session_destroy();
        return view('login');
    }

}