<?php

    if(!isset($_SESSION['loggedin'])){
        $router->get('','TaskController@getMyTasks');
        $router->get('login','LoginController@getLogin');
        $router->post('login','LoginController@postLogin');
        $router->get('logout','LoginController@logout');
    }else{
        $router->get('login','LoginController@getLogin');
        $router->post('login','LoginController@postLogin');
        $router->get('logout','LoginController@logout');

        $router->get('','TaskController@getMyTasks');

        $router->get('users','UsersController@index');
        $router->post('users','UsersController@insertUsers');

        $router->get('reassignindex','TaskController@reassignIndex');
        $router->get('reassign','TaskController@reassignTask');

        $router->get('addtask','TaskController@addTaskIndex');
        $router->post('addtask','TaskController@addTaskPost');

        $router->get('removeTask','TaskController@removeTask');
        $router->get('completeTask','TaskController@completeTask');
        $router->get('undoCompleteTask','TaskController@undoCompleteTask');
        $router->get('checkTask','TaskController@checkTask');

        $router->get('updateTask','TaskController@updateTask');
        $router->post('updateTask','TaskController@postUpdateTask');

        $router->get('mytasks','TaskController@getMyTasks');

    }
$router->get('pagenotfound','PagesController@pagenotfound');
//echo "test";
//echo "<pre>";
//var_dump($router->routes);
//echo "</pre>";

