<div class="container">
    <div class="row">
        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
            <h2>My Tasks</h2>
            <div class="table-responsive">

                <?php

                if (isset($success)){
                    foreach($success as $success){
                        echo "<div id=\"message\" class=\"alert alert-success\">". $success ."</div>";
                    }
                }

                ?>

                <table class="table table-striped" id="myTable">
                    <thead>
                    <tr>
                        <th>Task ID</th>
                        <th>Title</th>
                        <th>Created at</th>
                        <th>Last updated</th>
                        <th>Task Status</th>
                        <th>Options</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach($mytasks as $tasks=>$value){
                        echo "<tr>";

                        echo "<td>";
                        echo $value->idtask;
                        echo "</td>";

                        echo "<td>";
                        echo $value->title;
                        echo "</td>";

                        echo "<td>";
                        echo $value->created_at;
                        echo "</td>";

                        echo "<td>";
                        echo $value->updated_at;
                        echo "</td>";

                        echo "<td>";
                        if($value->completed){
                            echo "Completed";
                        }else{
                            echo "Pending";
                        }
                        echo "</td>";
                    ?>
                        <td>
                            <?php if(!$value->completed){?>
                                <a href="/completeTask?idtask=<?php echo $value->idtask; ?>" style="color:green">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </a>
                                <a href="/checkTask?idtask=<?php echo $value->idtask; ?>" style="color:green">
                                    <span class="glyphicon glyphicon-search"></span>
                                </a>
                                <a href="/updateTask?idtask=<?php echo $value->idtask; ?>" style="color:green">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="/reassignindex?idtask=<?php echo $value->idtask; ?>" style="color:orange">
                                    <span class="glyphicon glyphicon-user"></span>
                                </a>

                                <a href="/removeTask?idtask=<?php echo $value->idtask; ?>" style="color:red">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            <?php }else if($value->completed){?>
                                <a href="/checkTask?idtask=<?php echo $value->idtask; ?>" style="color:green">
                                    <span class="glyphicon glyphicon-search"></span>
                                </a>
                                <a href="/undoCompleteTask?idtask=<?php echo $value->idtask; ?>" style="color:orange">
                                    <span class="glyphicon glyphicon-refresh"></span>
                                </a>
                                <a href="/removeTask?idtask=<?php echo $value->idtask; ?>" style="color:red">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            <?php } ?>
                        </td>
                         <?php } ?>
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>