<div class="container">
    <div class="row">
        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
            <?php
            if (isset($success)){
                foreach($success as $success){
                    echo "<div id=\"message\" class=\"alert alert-success\">". $success ."</div>";
                }
            }
            ?>
            <div class="col-sm-12">
                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                        <label for="image">Image</label>

                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Select Image.. <input type="file" name="image" style="display: none;" multiple>
                                </span>
                            </label>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Task ID</label>
                                <input readonly required type="text" value="<?php echo $mytask->idtask;?>" placeholder="Task Title" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label>Assigned To</label>
                                <input readonly required type="text" value="<?php echo $mytask->assigned_to;?>" placeholder="Task Title" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Task Title</label>
                                <input required type="text" value="<?php echo $mytask->title;?>" name="title" placeholder="Task Title" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label>Description</label>
                                <textarea required class="form-control" name="description" rows="5" id="comment"><?php echo $mytask->description;?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Created At</label>
                                <input readonly required type="text" value="<?php echo $mytask->created_at;?>" placeholder="Task Title" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label>Last Updated At</label>
                                <input readonly required type="text" value="<?php echo $mytask->updated_at;?>" placeholder="Task Title" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Completed</label>
                                <?php if ($mytask->completed){?>
                                    <input readonly required type="text" value="Completed" placeholder="Task Title" class="form-control">
                                <?php }else{ ?>
                                    <input readonly required type="text" value="Pending"   placeholder="Task Title" class="form-control">
                                <?php } ?>
                            </div>
                        </div>
                        <input  required type="hidden" value="<?php echo $mytask->idtask;?>" name="idtask" placeholder="Task Title" class="form-control">
                        <button type="submit" id="submit" class="btn btn-primary">Update Task</button>
                    </form>
        </main>
    </div>
</div>
