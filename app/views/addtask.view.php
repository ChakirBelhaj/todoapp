<div class="container">
    <div class="row">
<main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
     <?php
     if (isset($success)){
         foreach($success as $success){
             echo "<div id=\"message\" class=\"alert alert-success\">". $success ."</div>";
         }
     }
     ?>
            <div class="col-sm-12">
                <div class="row">
                    <form method="post" enctype="multipart/form-data">
                        <label for="image">Image</label>

                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Select Image.. <input type="file" name="image" style="display: none;" multiple>
                                </span>
                            </label>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Task Title</label>
                                <input required type="text" name="title" placeholder="Task Title" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Description</label>
                                <textarea required class="form-control" name="description" rows="5" id="comment"></textarea>
                            </div>
                        </div>

                        <button type="submit" id="submit" class="btn btn-primary">Add Task</button>
                    </form>
 </main>
                </div>
            </div>
