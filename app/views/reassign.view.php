<div class="container">
    <div class="row">
<main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
    <h2>My Tasks</h2>
    <div class="table-responsive">
        <table class="table table-striped" id="myTable">
            <thead>
            <tr>

                <th>User ID</th>
                <th>Voornaam</th>
                <th>Achternaam</th>
                <th>Email</th>
                <th>Options</th>
            </tr>
            </thead>

            <tbody>
            <?php
            foreach($users as $tasks=>$value){
            echo "<tr>";

            echo "<td>";
            echo $value->iduser;
            echo "</td>";

            echo "<td>";
            echo $value->firstname;
            echo "</td>";

            echo "<td>";
            echo $value->lastname;
            echo "</td>";

            echo "<td>";
            echo $value->email;
            echo "</td>";
            ?>
            <td>
                    <a href="/reassign?idtask=<?php echo $_GET['idtask']; ?>&iduser=<?php echo $value->iduser; ?>" class="btn btn-warning btn-sm">
                        <span class="glyphicon glyphicon-user">Reassign</span>
                    </a>
            </td>
            <?php } ?>
            </tbody>
        </table>
    </div>
</main>
</div>
</div>
