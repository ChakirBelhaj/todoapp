<div class="container">
    <div class="row">
        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
            <div class="col-sm-12">
                <div class="row">
                    <?php if(isset($mytask->image)){ ?>
                    <img alt="Embedded Image" src="data:image/png;base64,<?php echo $mytask->image; ?>" width="400" height="400" />
                    <?php } ?>
                    <form method="post">
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Task ID</label>
                                <input readonly required type="text" value="<?php echo $mytask->idtask;?>" name="title" placeholder="Task Title" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label>Assigned To</label>
                                <input readonly required type="text" value="<?php echo $mytask->assigned_to;?>" name="title" placeholder="Task Title" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Task Title</label>
                                <input readonly required type="text" value="<?php echo $mytask->title;?>" placeholder="Task Title" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label>Description</label>
                                <textarea readonly class="form-control" name="description" rows="5" id="comment"><?php echo $mytask->description;?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Created At</label>
                                <input readonly required type="text" value="<?php echo $mytask->created_at;?>" name="title" placeholder="Task Title" class="form-control">
                            </div>
                            <div class="col-sm-5 form-group">
                                <label>Last Updated At</label>
                                <input readonly required type="text" value="<?php echo $mytask->updated_at;?>" name="title" placeholder="Task Title" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 form-group">
                                <label>Completed</label>
                                <?php if ($mytask->completed){?>
                                    <input readonly required type="text" value="Completed" name="title" placeholder="Task Title" class="form-control">
                                <?php }else{ ?>
                                    <input readonly required type="text" value="Pending" name="title" placeholder="Task Title" class="form-control">
                                <?php } ?>
                            </div>
                        </div>
                    </form>
        </main>
    </div>
</div>
