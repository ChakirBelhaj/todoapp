<!doctype html>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
    <title>TODOAPP</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/yeti/bootstrap.min.css"">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
    <link href="public/css/sidebar.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div id="wrapper" class="toggled">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#">
                    Start Bootstrap
                </a>
            </li>
            <li>
                <a href="mytasks">My Tasks</a>
            </li>
            <li>
                <a href="addtask">Add Task</a>
            </li>
            <li>
                <a href="logout">Logout</a>
            </li>
        </ul>
    </div>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <a href="#menu-toggle" class="glyphicon glyphicon-th" id="menu-toggle"></a>
        </div>
    </div>





