<?php
use Todo\Core\Request;
use Todo\Core\Router;

session_start();

if($_SERVER['REQUEST_URI'] != "/login" && $_SERVER['REQUEST_URI'] !=  "/logout" && $_SERVER['REQUEST_URI'] !=  "/pagenotfound") {
    require "public/header.php";
}else{
    require "public/headernotloggedin.php";
}
require 'vendor/autoload.php';
require 'core/handler.php';



//load routes.php and directs trafic
Router::load('app/routes.php')
    ->direct(Request::uri(),
    Request::method()

    );

require "public/footer.php";

?>