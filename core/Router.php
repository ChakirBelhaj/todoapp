<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/20/2017
 * Time: 12:56 AM
 */

namespace Todo\Core;


class Router
{
    //routes array
    public $routes = [

        'GET'=>[],
        'POST'=>[]

    ];
//    retuns r
    public static function load($file){
//      creates router instance with  'GET'=>[] 'POST'=>[]     arrays.
        $router = new static;
        //require routes.php
        require $file;
        return $router;
    }
    //get route, gets $uri and controller
    public function get($uri,$controller){
        //redirects the uri to the controller function
        $this->routes['GET'][$uri] = $controller;

    }
    //post route gets uri and controller
    public function post($uri,$controller){
        //redirects the uri to the controller function
        //the uri will now run the selected controller code
        $this->routes['POST'][$uri] = $controller;

    }
    //directs route
    public function direct($uri, $requestType){

        //checks if array key  of routes exists.
        if (array_key_exists($uri, $this->routes[$requestType])){
//            return $this->routes[$requestType][$uri];

            //same as   $test = explode('@', $this->routes[$requestType][$uri]);
//                      return $this->callAction($test[0],$test[1]);
            //splits string by @
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri]));
        }

        header("Location: /pagenotfound ");

        throw new Exception('No route definde for this URI ');
    }

    public function callAction($controller, $action){
//        die(var_dump($controller,$action));

/*        gets controller from map todo controllers controllername */
        $controller = "Todo\\Controllers\\{$controller}";

        //controller becomes object
        $controller = new $controller;

        //if controller does not exist
        if(! method_exists($controller, $action)){
            throw new Exception(
                "{$controller} does not respond to the {$action} action "
            );
        }
        //returns the function in the controller
        return $controller->$action();
    }

}