<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/22/2017
 * Time: 3:03 AM
 */

namespace Todo\Core;

class App
{

    protected static $registry = [];

    //adds key and value to $registry array
    public static function bind($key, $value){

        static::$registry[$key] = $value;

    }

    //adds key to registry array
    public static function get($key){

        if(! array_key_exists($key, static::$registry)){
            throw new Exception("no {$key} is bound in the container.");
        }

        return static::$registry[$key];

    }
}