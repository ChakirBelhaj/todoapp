<?php
use Todo\Core\App;
//binds config to App
App::bind('config', require 'config.php');

//binds database to App
//add config to pdo connection
App::bind('database', new QueryBuilder(
    Connection::connect(App::get('config')['database'])
));


//view redirects
function view($name, $data = []){
    //creates variable $data
    extract($data);
    //returns view
    return require "app/views/{$name}.view.php";
}

//helper function to redirect
function redirect($path){
    header("Location: /{$path}");
}

//helper function for debugging
function precho($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}