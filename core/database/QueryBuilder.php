<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/16/2017
 * Time: 5:50 PM
 */

class QueryBuilder
{

    protected $pdo;

    public function __construct($pdo)
    {

        $this->pdo = $pdo;

    }
    // select all from table
    public function selectall($table){

        $statement = $this->pdo->prepare("select * from {$table}");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);

    }
    //select all data from users
    public function selectUser($email){

        $statement = $this->pdo->prepare("select * from users where email =  '{$email}'");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);

    }
    //select all from table where id user is not $loggedinuser ( this is needed for the reassign task table )
    public function selectAllExceptFromLoggedinUser($table,$loggedinuser){

        $statement = $this->pdo->prepare("select * from {$table} WHERE iduser NOT IN ( $loggedinuser )");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);

    }
    //selects all task for the given user.
    public function selectMyTasks($table,$userid){

        $statement = $this->pdo->prepare("select * from $table where assigned_to = '$userid'");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);

    }
    //selects task on id
    public function selectTaskOnId($table,$idtask){

        $statement = $this->pdo->prepare("select * from $table where idtask = '$idtask'");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);

    }
    //deletes from task
    public function deleteFromTask($table,$idtask){

        $statement = $this->pdo->prepare("DELETE from $table where idtask = '$idtask'");

        try{
            $statement->execute();
        }catch(Exception $e){
            echo "something went wrong";
        }
    }
    //completes task
    public function completeTask($table,$idtask,$date){

        $statement = $this->pdo->prepare("UPDATE $table SET completed='1', updated_at='$date', completed_at='$date' WHERE idtask='$idtask'");

        try{
            $statement->execute();
        }catch(Exception $e){
            echo "something went wrong";
        }
    }
    //undo completed task
    public function undoCompleteTask($table,$idtask,$date){

        $statement = $this->pdo->prepare("UPDATE $table SET completed='0', updated_at='$date', completed_at= NULL WHERE idtask='$idtask'");

        try{
            $statement->execute();
        }catch(Exception $e){
            echo "something went wrong";
        }
    }
    //update task
    public function updateTask($table,$idtask,$title,$description,$date,$image){
        if(empty($image)){
        $statement = $this->pdo->prepare("UPDATE $table SET  title='$title', description='$description', updated_at='$date' WHERE idtask='$idtask'");
        }else{
            $statement = $this->pdo->prepare("UPDATE $table SET  title='$title', description='$description', updated_at='$date', image='$image' WHERE idtask='$idtask'");
        }
        try{
            $statement->execute();
        }catch(Exception $e){
            echo "something went wrong";
        }
    }
    //reassigns task to user
    public function reassignTask($table,$idtask,$iduser,$date){

        $statement = $this->pdo->prepare("UPDATE $table SET  assigned_to='$iduser', updated_at='$date' WHERE idtask='$idtask'");

        try{
            $statement->execute();
        }catch(Exception $e){
            echo "something went wrong";
        }
    }


    //inserts array into database
    public function insert($table, $parameters){
        //declares a string with placeholders
        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $table,
            //gets all the keys from array $parameters adds ', ' between each key
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );
        try{
        $statement = $this->pdo->prepare($sql);

        $statement->execute($parameters);
        }catch(Exception $e){
            echo "something went wrong";
        }

    }
}