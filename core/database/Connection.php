<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/16/2017
 * Time: 5:55 PM
 */

class Connection
{
    //pdo connnection
    public static function connect($config) {

//        $servername = "localhost:3306"; //37.97.223.88
//        $username = "root";
//        $password = "darkmaster";
//        $dbname = "todos";

        try{
//            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $conn = new PDO(
                "mysql:host=" . $config['connection'] . ";dbname=".$config['dbname'],
                $config['username'],
                $config['password'],
                $config['options']
            );
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $conn;
        }


        catch(PDOException $e) {
            echo "Connection Failed: " . $e->getMessage();
            return NULL;
        }
    }

}