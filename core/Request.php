<?php
/**
 * Created by PhpStorm.
 * User: Abstract Build
 * Date: 10/20/2017
 * Time: 1:28 AM
 */

namespace Todo\Core;

class Request
{
    //returns trimmed uri
    public static function uri(){

        //gets URI form superglobal and trims /
        //Parse a URL and return its components
        return trim(parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH), '/');

    }
    //returns request methode
    public static function method(){

        return $_SERVER['REQUEST_METHOD'];

    }
}